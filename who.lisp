;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package:CL-WHO; Base: 10 -*-
;;; $Header: /usr/local/cvsrep/cl-who/who.lisp,v 1.16 2004/04/24 00:05:39 edi Exp $

;;; Copyright (c) 2003-2004, Dr. Edmund Weitz. All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package #:cl-who)

(defvar *prologue*
  "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">"
  "This is the first line that'll be printed if the :PROLOGUE keyword
argument is T")

(defparameter *escape-char-p*
  #'(lambda (char)
      (or (find char "<>&'\"")
          (> (char-code char) 127)))
  "Used by ESCAPE-STRING to test whether a character should be escaped.")

(defparameter *indent* nil
  "Whether to insert line breaks and indent. Also controls amount of
indentation dynamically.")

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (boundp '+newline+)
    (defconstant +newline+ (make-string 1 :initial-element #\Newline)
      "Used for indentation."))

  (unless (boundp '+spaces+)
    (defconstant +spaces+ (make-string 2000 :initial-element #\Space)
      "Used for indentation.")))

(defmacro n-spaces (n)
  "A string with N spaces - used by indentation."
  `(make-array ,n
	      :element-type 'base-char
	      :displaced-to +spaces+
	      :displaced-index-offset 0))

(defun escape-string (string &key (test *escape-char-p*))
  (declare (optimize speed))
  "Escape all characters in STRING which pass TEST. This function is
not guaranteed to return a fresh string."
  (let ((first-pos (position-if test string)))
    (if (not first-pos)
      ;; nothing to do, just return STRING
      string
      (with-output-to-string (s)
        (loop with len = (length string)
              for old-pos = 0 then (1+ pos)
              for pos = first-pos
                  then (position-if test string :start old-pos)
              ;; now the characters from OLD-POS to (excluding) POS
              ;; don't have to be escaped while the next character has to
              for char = (and pos (char string pos))
              while pos
              do (write-sequence string s :start old-pos :end pos)
                 (case char
                   ((#\<)
                     (write-sequence "&lt;" s))
                   ((#\>)
                     (write-sequence "&gt;" s))
                   ((#\&)
                     (write-sequence "&amp;" s))
                   ((#\')
                     (write-sequence "&#039;" s))
                   ((#\")
                     (write-sequence "&quot;" s))
                   (otherwise
                     (format s "&#x~x;" (char-code char))))
              while (< (1+ pos) len)
              finally (unless pos
                        (write-sequence string s :start old-pos)))))))

(defun escape-string-minimal (string)
  "Escape only #\<, #\>, and #\& in STRING."
  (escape-string string :test #'(lambda (char) (find char "<>&"))))

(defun escape-string-minimal-plus-quotes (string)
  "Like ESCAPE-STRING-MINIMAL but also escapes quotes."
  (escape-string string :test #'(lambda (char) (find char "<>&'\""))))

(defun escape-string-iso-8859 (string)
  "Escapes all characters in STRING which aren't defined in
ISO-8859. This of course assumes that STRING is an ISO-8859 string."
  (escape-string string :test #'(lambda (char)
                                  (or (find char "<>&'\"")
                                      (> (char-code char) 255)))))

(defun escape-string-all (string)
  "Escapes all characters in STRING which aren't in the 7-bit ASCII
character set."
  (escape-string string :test #'(lambda (char)
                                  (or (find char "<>&'\"")
                                      (> (char-code char) 127)))))

(defun print-tag (list)
  (declare (optimize speed space))
  "Returns a string list corresponding to the HTML tag TAG. Utility
function used by TREE-TO-TEMPLATE."
  (let (tag attr-list body)
    (cond
      ((atom (first list))
       (setq tag (first list))
       ;; collect attribute/value pairs into ATTR-LIST and tag body (if
       ;; any) into BODY
       (loop for rest on (cdr list) by #'cddr
	     if (keywordp (first rest))
	       collect (cons (first rest) (second rest)) into attr
	     else
	       do (progn (setq attr-list attr)
			 (setq body rest)
			 (return))
	     finally (setq attr-list attr)))
      ((listp (first list))
       (setq tag (first (first list)))
       (loop for rest on (cdr (first list)) by #'cddr
	     if (keywordp (first rest))
	     collect (cons (first rest) (second rest)) into attr
	     finally (setq attr-list attr))
       (setq body (cdr list))))

    (nconc
     (if *indent*
       ;; indent by *INDENT* spaces
       (list +newline+ (n-spaces *indent*)))
     ;; tag name
     (list "<" (string-downcase tag))
     ;; attributes
     (loop with =var= = (gensym)
           for (attr . val) in attr-list
           unless (null val) ;; no attribute at all if VAL is NIL
           if (constantp val)
           nconc (list " "
                       ;; name of attribute
                       (string-downcase attr)
                       "='"
                       ;; value of attribute
                       (cond ((stringp val)
                               ;; a string, just use it - this case is
                               ;; actually not necessary because of
                               ;; the last case
                               val)
                             ((eq val t)
                               ;; VAL is T, use attribute's name
                               (string-downcase attr))
                             (t
                               ;; constant form, PRINC it
                               (format nil "~A" val)))
                       "'")
           else
           ;; do the same things as above but at runtime
           nconc (list `(let ((,=var= ,val))
                         (cond ((null ,=var=))
                               ((eq ,=var= t)
                                 (htm ,(format nil " ~A='~A'"
                                               (string-downcase attr)
                                               (string-downcase attr))))
                               (t
                                 (htm ,(format nil " ~A='" (string-downcase attr))
                                      (str ,=var=)
                                      "'"))))))
     (if body
       (append
        (list ">")
        ;; now hand over the tag's body to TREE-TO-TEMPLATE, increase
        ;; *INDENT* by 2 if necessary
        (if *indent*
          (let ((*indent* (+ 2 *indent*)))
            (tree-to-template body))
          (tree-to-template body))
        (if *indent*
          ;; indentation
          (list +newline+ (n-spaces *indent*)))
        ;; closing tag
        (list "</" (string-downcase tag) ">"))
       ;; no body, so no closing tag
       (list " />")))))

(defun apply-to-tree (function test tree)
  (declare (optimize speed space))
  (declare (type function function test))
  "Apply FUNCTION recursively to all elements of the tree TREE \(not
only leaves) which pass TEST."
  (cond
    ((funcall test tree)
      (funcall function tree))
    ((consp tree)
      (cons
       (apply-to-tree function test (car tree))
       (apply-to-tree function test (cdr tree))))
    (t tree)))

(defun replace-htm (tree transformation)
  (declare (optimize speed space))
  "Replace all subtrees of TREE starting with the symbol HTM with the
same subtree after TRANSFORMATION has been applied to it. Utility
function used by TREE-TO-TEMPLATE and TREE-TO-COMMANDS-AUX."
  (apply-to-tree #'(lambda (element)
                     (cons 'htm (funcall transformation (cdr element))))
                 #'(lambda (element)
                     (and (consp element)
                          (eq (car element) 'htm)))
                 tree))

(defun tree-to-template (tree)
  "Transforms an HTML tree into an intermediate format - mainly a
flattened list of strings. Utility function used by TREE-TO-COMMANDS-AUX."
  (loop for element in tree
        nconc (cond ((keywordp element)
                      ;; stand-alone tag without content
                      (list (concatenate 'string
                                         (if *indent*
                                           (concatenate 'string
                                                        +newline+
                                                        (n-spaces *indent*)))
                                         "<"
                                         (string-downcase element)
                                         " />")))
                    ((or
		      (and (listp element)
			   (keywordp (first element)))
		      (and (listp element)
			   (listp (first element))
			   (keywordp (first (first element)))))
                      ;; normal tag
                      (print-tag element))
                    ((listp element)
                      ;; most likely a normal Lisp form - check
                      ;; if we have nested HTM subtrees
                      (list
                       (replace-htm element #'tree-to-template)))
                    (t
                      (if *indent*
                        (list +newline+ (n-spaces *indent*) element)
                        (list element))))))

(defun string-list-to-string (string-list)
  (declare (optimize speed space))
  "Concatenates a list of strings to one string."
  ;; note that we can't use APPLY with CONCATENATE here because of
  ;; CALL-ARGUMENTS-LIMIT
  (let ((total-size 0))
    (dolist (string string-list)
      (incf total-size (length string)))
    (let ((result-string (make-sequence 'simple-string total-size))
          (curr-pos 0))
      (dolist (string string-list)
        (replace result-string string :start1 curr-pos)
        (incf curr-pos (length string)))
      result-string)))

(defun conc (&rest string-list)
  "Concatenates all arguments which should be string into one string."
  (funcall #'string-list-to-string string-list))

(defun tree-to-commands-aux (tree stream)
  (declare (optimize speed space))
  "Transforms the intermediate representation of an HTML tree into
Lisp code to print the HTML to STREAM. Utility function used by
TREE-TO-COMMANDS."
  (let ((in-string t)
        collector
        string-collector)
    (flet ((emit-string-collector ()
             "Generate a WRITE-STRING statement for what is currently
in STRING-COLLECTOR."
             (list 'write-string
                   (string-list-to-string (nreverse string-collector))
                   stream))
           (tree-to-commands-aux-internal (tree)
             "Same as TREE-TO-COMMANDS-AUX but with closed-over STREAM
for REPLACE-HTM."
             (tree-to-commands-aux tree stream)))
      (unless (listp tree)
        (return-from tree-to-commands-aux tree))
      (loop for element in tree
            do (cond ((and in-string (stringp element))
                       ;; this element is a string and the last one
                       ;; also was (or this is the first element) -
                       ;; collect into STRING-COLLECTOR
                       (push element string-collector))
                     ((stringp element)
                       ;; the last one wasn't a string so we start
                       ;; with an empty STRING-COLLECTOR
                       (setq string-collector (list element)
                             in-string t))
                     (string-collector
                       ;; not a string but STRING-COLLECTOR isn't
                       ;; empty so we have to emit the collected
                       ;; strings first
                       (push (emit-string-collector) collector)
                       (setq in-string nil
                             string-collector '())
                       ;; collect this element but walk down the
                       ;; subtree first
                       (push (replace-htm element #'tree-to-commands-aux-internal)
                             collector))
                     (t
                       ;; not a string and empty STRING-COLLECTOR
                       (push (replace-htm element #'tree-to-commands-aux-internal)
                             collector)))
            finally (return (if string-collector
                              ;; finally empty STRING-COLLECTOR if
                              ;; there's something in it
                              (nreverse (cons (emit-string-collector)
                                              collector))
                              (nreverse collector)))))))

(defun tree-to-commands (tree stream prologue)
  (declare (optimize speed space))
  "Transforms an HTML tree into code to print the HTML to STREAM."
  ;; use TREE-TO-TEMPLATE, then TREE-TO-COMMANDS-AUX, and finally
  ;; replace the special symbols ESC, STR, FMT, and HTM
  (apply-to-tree #'(lambda (x)
                     (case (first x)
                       ((esc)
                         ;; (ESC form ...) ->
                         ;; (WRITE-STRING (ESCAPE-STRING form STREAM))
                         (list 'write-string
                               (list 'escape-string
                                     (second x))
                               stream))
                       ((str)
                         ;; (STR form ...) --> (PRINC form STREAM)
                         `(princ ,(second x) ,stream))
                       ((fmt)
                         ;; (FMT form*) --> (FORMAT STREAM form*)
                         (cons 'format
                               (cons stream (rest x))))))
                 #'(lambda (x)
                     (and (consp x)
                          (member (first x)
                                  '(esc str fmt)
                                  :test #'eq)))
                 ;; wrap PROGN around the HTM forms
                 (apply-to-tree (constantly 'progn)
                                #'(lambda (x)
                                    (and (atom x)
                                         (eq x 'htm)))
                                (tree-to-commands-aux
                                 (cons 'htm
                                       (cons prologue
                                             (tree-to-template tree)))
                                 stream))))

(defmacro with-html-output ((var &optional stream
                                 &key prologue
                                      ((:indent *indent*) *indent*))
                            &body body)
  "Transform the enclosed BODY consisting of HTML as s-expressions
into Lisp code to write the corresponding HTML as strings to VAR -
which should either hold a stream or which'll be bound to STREAM if
supplied."
  (when (and *indent*
             (not (integerp *indent*)))
    (setq *indent* 0))
  (when (eq prologue t)
    (setq prologue *prologue*))
  `(let ((,var ,(or stream var)))
    ,(tree-to-commands body var prologue)))

(defmacro with-html-output-to-string ((var &optional string-form
                                           &key (element-type ''character)
                                                prologue
                                                indent)
                                      &body body)
  "Transform the enclosed BODY consisting of HTML as s-expressions
into Lisp code which creates the corresponding HTML as a string."
  `(with-output-to-string (,var ,string-form
                                #-(or :ecl :cmu :sbcl) :element-type
                                #-(or :ecl :cmu :sbcl) ,element-type)
    (with-html-output (,var nil :prologue ,prologue :indent ,indent)
      ,@body)))

(defmacro show-html-expansion ((var &optional stream
                                    &key prologue
                                         ((:indent *indent*) *indent*))
                               &body body)
  "Show the macro expansion of WITH-HTML-OUTPUT."
  (when (and *indent*
             (not (integerp *indent*)))
    (setq *indent* 0))
  (when (eq prologue t)
    (setq prologue *prologue*))
  `(pprint '(let ((,var ,(or stream var)))
             ,(tree-to-commands body var prologue))))

;; stuff for Nikodemus Siivola's HYPERDOC
;; see <http://common-lisp.net/project/hyperdoc/>
;; and <http://www.cliki.net/hyperdoc>

(defvar *hyperdoc-base-uri* "http://weitz.de/cl-who/")

(let ((exported-symbols-alist
       (loop for symbol being the external-symbols of :cl-who
             collect (cons symbol
                           (concatenate 'string
                                        "#"
                                        (string-downcase symbol))))))
  (defun hyperdoc-lookup (symbol type)
    (declare (ignore type))
    (cdr (assoc symbol
                exported-symbols-alist
                :test #'eq))))
               